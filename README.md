# Firmware System Tests

This repo provides tools for automating system (end to end) tests for OpenWrt based firmwares.

At this initial stage, there are only tests for the LibreMesh/Lime-App/LibreRouter. Eventually the tests will be split from the repo so it can be reused by others (for example to write stock OpenWrt/LuCi tests).

## Tools provided

* Qemu: to run the embedded system
* Pexpect: to run tests to a serial port of the system
* Selenium + Firefox: to run web tests
* Pytest: for running and writing tests
* Docker image: everything packed to be run headless in a server
* Gitlab CI: CI integration to run the automation in docker runners

## Status

Although this system is being used in production, we consider that the architecture is not stable and may change as we build more components and tests.

## Usage

### With docker

```
export FW_RAMFS=/path/to/the-x86-64-ramfs.bzImage
export FW_ROOTFS=/path/to/the-x86-64-generic-rootfs.tar.gz
docker run --rm -it --device=/dev/kvm:/dev/kvm --device=/dev/net/tun:/dev/net/tun -v$PWD:/home/runner -v$FW_ROOTFS:/home/runner/fw_rootfs \
	-v$FW_RAMFS:/home/runner/fw_ramfs --cap-add NET_ADMIN -e PYTHONPATH=. -e QEMU_ARGS='fw_rootfs fw_ramfs' librerouter/fwsystests:1.0 pytest -vs
```


### Without docker

```
PYTHONPATH=. SELENIUM_NOHEADLESS=1 QEMU_ARGS="path/to/fw-x86-64-generic-rootfs.tar.gz path/to/fw-x86-64-ramfs.bzImage" pytest -vs
```

### Continuous integration

One way to use it to test binaries built by a previous CI step (using gitlab-ci syntax):
```
full_build:
  ... # build the firmware

system_tests:
  stage: test
  image: "librerouter/fwsystests:1.0"
  script:
    - wget -q https://gitlab.com/librerouter/fwsystests/-/archive/master/fwsystests-master.tar.gz
    - tar xf fwsystests-master.tar.gz
    - cd fwsystests-master
    - FW_ROOTFS=$(readlink -f ../bin/targets/x86/64/*rootfs.tar.gz)
    - FW_RAMFS=$(readlink -f ../bin/targets/x86/64/*.bzImage)
    - QEMU_ARGS="$FW_ROOTFS $FW_RAMFS" PYTHONPATH=. pytest --verbose
```

## Writing tests


Start from a simple test with a breakpoint (or using pdb or ipdb with `ipdb;ipdb.set_trace()`):

```

def test_foo(guest, browser):
    breakpoint()
```

Run the test using `pytest -vs -k test_foo`, it will start the guest in qemu, launch a firefox windown and start
a python interpreter. (note use the `SELENIUM_NOHEADLESS=1` env var so that the Firefox window is visible).

Use the python interpreter to interact with the broser. It is very useful to use the Web developer tools in Firefox
to view the DOM objects. Use selenium through the browser object in the python terminal:

```
ipdb> browser.find_element_by_css_selector("input")
<selenium.webdriver.firefox.webelement.FirefoxWebElement (session="fc5a2994-caa4-4e86-be08-4d44bfcd6b21", element="f451b5b2-54b2-4301-ac35-0cfc978f657e")>
```
Do the asserts in the interpreter and the copy the useful parts of the test to the test function. Iterate until you
have a working test. Take in consideration that the test will run at full speed so you may have to wait for
modifications done in the DOM using `browser.wait_until_css_selector` and friends.
