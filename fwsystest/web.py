# Copyright (C) 2020 Santiago Piccinini <spiccinini@altermundi.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3,
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait

TIMEOUT_S = 10

class Browser(webdriver.Firefox):
    def wait_until_css_selector(self, selector, timeout_s=TIMEOUT_S):
        return self._wait_until(lambda x: self.find_element_by_css_selector(selector), timeout_s)

    def wait_until_xpath_selector(self, selector, timeout_s=TIMEOUT_S):
        return self._wait_until(lambda x: self.find_element_by_xpath(selector), timeout_s)

    def _wait_until(self, func, timeout_s):
        return WebDriverWait(self, timeout_s).until(func)


def start_browser(headless=False, debug=False):
    options = Options()
    options.set_preference('intl.accept_languages', 'en-US, en')
    if headless:
        options.add_argument('-headless')
    if debug:
        options.log.level = "trace" # logs to geckodriver.log
    browser = Browser(options=options)
    return browser


