# Copyright (C) 2020 Santiago Piccinini <spiccinini@altermundi.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3,
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

import atexit
import shlex
import subprocess
import time
import os
import pexpect
import socket
import urllib.request
import urllib.error

THIS_FILE_DIR_PATH = os.path.abspath(os.path.dirname(__file__))

class QemuBootError(Exception):
    pass

class QemuHandler:
    """
    Spawns, stops and provides a pexpect console for a qemu system.
    """
    STOP_TIMEOUT_S = 10
    MIN_BOOT_TIME_S = 2
    SERIAL_TELNET_PORT = 45460
    SERIAL_TELNET_TIMEOUT_S = 20
    BOOT_TIMEOUT_S = 25

    def __init__(self, start_args):
        self.console = None
        self.qemu_process = None
        self.start_args = start_args

    def start(self):
        if self.is_running():
            raise ValueError("Qemu is already running")
        qemu_start_path = os.path.join(THIS_FILE_DIR_PATH, "..", "tools", "qemu_dev_start")
        qemu_start_cmd = f"sudo {qemu_start_path} --no-serial {self.start_args}"
        self.qemu_process = subprocess.Popen(shlex.split(qemu_start_cmd))
        atexit.register(self.stop)

        for i in range(self.SERIAL_TELNET_TIMEOUT_S * 10):
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex(('127.0.0.1', self.SERIAL_TELNET_PORT))
            if result == 0:
                sock.close()
                break
            time.sleep(0.1)
        else:
            raise QemuBootError("Can't connec to to the serial port")

        self.get_console()
        self.wait_guest_untill_boots()
        return self.qemu_process

    def wait_guest_untill_boots(self):
        start_time = time.time()

        time_remaining_s = self.BOOT_TIMEOUT_S - (time.time() - start_time)
        while True:
            try:
                r = urllib.request.urlopen("http://10.13.0.1/boot-ended", timeout=time_remaining_s)
                if r.status == 200:
                    break
            except urllib.error.URLError: # happend normally when the interfaces are going up
                pass

            time_remaining_s = self.BOOT_TIMEOUT_S - (time.time() - start_time)
            if time_remaining_s < 0:
                raise QemuBootError("Can't verify that the system finished booting")

    def stop(self):
        """
        Stop gracefully the qemu process, waiting until powerdown.
        """
        if not self.qemu_process:
            return
        if self.console:
            self.console.terminate()
        subprocess.run("./tools/qemu_dev_stop", capture_output=True)
        try:
            self.qemu_process.communicate(timeout=self.STOP_TIMEOUT_S)
            self.qemu_process = None
        except subprocess.TimeoutExpired:
            print('Qemu did not terminate in time')

    def is_running(self):
        return self.qemu_process is not None

    def get_console(self):
        if self.console is None:
            # telnet to the qemu telnet server into the serial console
            # not a telnet server running in the guest
            self.console = pexpect.spawn("telnet 127.0.0.1 %d" % self.SERIAL_TELNET_PORT,
                                         timeout=self.SERIAL_TELNET_TIMEOUT_S)
            self.console.expect("Please press Enter to activate this console")
            self.console.sendline("")
            self.console.sendline("export PS1='prompt>>'")
            self.console.expect("prompt>>")
        return self.console
