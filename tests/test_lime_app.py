# Copyright (C) 2020 Santiago Piccinini <spiccinini@altermundi.net>
# Copyright (C) 2020 Germán Ferrero <gferrero@altermundi.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3,
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

import os
import tempfile
import time

import pytest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from fwsystest.web import start_browser
from fwsystest.qemu import QemuHandler

NODE_IP = "10.13.0.1"

# use scope "module" to speed up tests when they are idempotent (group them in the same test file)
@pytest.fixture(scope="function")
def guest(request):
    guest = QemuHandler(start_args=os.environ['QEMU_ARGS'])
    guest.start()
    request.addfinalizer(lambda : guest.stop())
    return guest

@pytest.fixture
def browser(request):
    from selenium.common.exceptions import WebDriverException
    browser = start_browser(headless=not os.environ.get('SELENIUM_NOHEADLESS', False))
    try:
        browser.get(f"http://{NODE_IP}")
        # Wait for the app to load
        WebDriverWait(browser, 10).until(EC.title_is("LiMe"))
        disable_css_transitions(browser)
        close_fbw_splash(browser)
    except WebDriverException:
        browser.quit()
        raise
    request.addfinalizer(lambda : browser.quit())
    return browser

def navigate_to_section(browser, section):
    menu = browser.find_element_by_tag_name("nav")

    if not menu.is_displayed():
        menu_btn = browser.find_element_by_css_selector("div[class^='hamburger']")
        menu_btn.click()
        assert menu.is_displayed()

    # wait until
    section_link = menu.find_element_by_link_text(section)
    section_link.click()

def get_element_center(element):
    center_x = element.location['x'] + element.size['width'] / 2
    center_y = element.location['y'] + element.size['height'] / 2
    return (center_x, center_y)

def disable_css_transitions(browser):
    """Add a CSS rule that removes all transitions CSS"""
    # CSS transitions slows down the interaction
    script = """
    const styleElement = document.createElement('style');
    styleElement.setAttribute('id','style-tag');
    const styleTagCSSes = document.createTextNode('*,:after,:before{-webkit-transition:none!important;-moz-transition:none!important;-ms-transition:none!important;-o-transition:none!important;transition:none!important;}');
    styleElement.appendChild(styleTagCSSes);
    document.head.appendChild(styleElement);
    """
    browser.execute_script(script)

def close_fbw_splash(browser):
    fbw_btn_cancel = browser.find_elements_by_tag_name("button")[1]
    assert fbw_btn_cancel.text == 'CANCEL'
    fbw_btn_cancel.click()

def login(browser, password="this is a g00d passw0rd"):
    password_input = browser.find_element_by_css_selector("input[name='password']")
    password_input.send_keys(password)
    login_btn = browser.find_element_by_css_selector("button")
    assert login_btn.text == "LOGIN"
    login_btn.click()

def get_screen_text(browser):
    return browser.find_element_by_class_name("container").text

def test_is_linux(guest):
    console = guest.get_console()
    console.sendline("uname -a")
    console.expect("GNU/Linux")

def test_lime_app_map(guest, browser):
    navigate_to_section(browser, 'Map')
    # As this node has not been located yet. A locate my node shows button shows up
    locate_my_node_btn = browser.wait_until_xpath_selector('//button[text()="locate my node"]')
    locate_my_node_btn.click()
    # A location-marker should show up in the middle of the map
    location_marker = browser.wait_until_xpath_selector('//*[@id="location-marker"]')
    map_div = browser.find_element_by_id('map-container')
    map_center_x, map_center_y = get_element_center(map_div)
    marker_center_x, marker_center_y = get_element_center(location_marker)
    # ignore rounding issues
    assert (abs(map_center_x - marker_center_x) <= 1)
    assert (abs(map_center_y - marker_center_y) <= 1)
    confirm_location_btn = browser.wait_until_xpath_selector('//button[text()="confirm location"]')
    confirm_location_btn.click()

    # go out of the map and come back
    navigate_to_section(browser, "Status")
    navigate_to_section(browser, "Map")

    # Now it should shows the node in the middle of the screen
    node_marker = browser.wait_until_xpath_selector('//img[@alt="node marker"]')
    marker_center_x, marker_center_y = get_element_center(node_marker)
    assert (abs(map_center_x - marker_center_x) <= 1)
    # Node marker is shown above node coords
    marker_bottom = marker_center_y + (node_marker.size['height'] / 2)
    assert (abs(map_center_y - marker_bottom) <= 1)
    # Now it shows a button to edit the location
    browser.wait_until_xpath_selector('//button[text()="edit location"]')
    # TODO test community view

def test_lime_app_network_administration(guest, browser):
    navigate_to_section(browser, "Network Configuration")

    # This lead us to the network configuration

    # wait for the login to complete
    browser.wait_until_css_selector("input[placeholder='Password']")

    password_input = browser.find_element_by_css_selector("input[placeholder='Password']")
    re_enter_password = browser.find_element_by_css_selector(
        "input[placeholder='Re-enter Password']")

    change_btn = browser.find_element_by_xpath("//button[text()='Change']")
    assert not change_btn.is_enabled()

    password_input.send_keys("123")
    assert not change_btn.is_enabled()

    re_enter_password.send_keys("123")
    assert not change_btn.is_enabled()

    password_input.clear()
    password_input.send_keys("this is a g00d passw0rd")
    assert not change_btn.is_enabled()

    re_enter_password.clear()
    re_enter_password.send_keys("this is a g00d passw0rd")
    assert change_btn.is_enabled()
    change_btn.click()

    WebDriverWait(browser, 10).until(lambda x: 'Shared Password changed successfully' in
                                               get_screen_text(browser))

    # go to Status and back
    navigate_to_section(browser, "Status")
    navigate_to_section(browser, "Network Configuration")

    # now it should not prompt for the password as we are already logged in this session
    assert "Change Shared Password" in get_screen_text(browser)

    # reload the app to check that now the password is required
    browser.refresh()
    browser.wait_until_xpath_selector('//button[text()="Cancel"]')
    close_fbw_splash(browser)

    # test an incorrect password
    password_input = browser.find_element_by_css_selector("input[name='password']")
    password_input.send_keys("incorrectpassword")  # this asumes starting without password
    login_btn = browser.find_element_by_css_selector("button")
    login_btn.click()
    assert "Wrong password, try again" in get_screen_text(browser)

    # now with the real password
    password_input.clear()
    password_input.send_keys("this is a g00d passw0rd")
    login_btn.click()
    assert "Change Shared Password" in get_screen_text(browser)

def test_firmware_upgrade(guest, browser):
    navigate_to_section(browser, 'Firmware')

    inpt = browser.wait_until_css_selector("input[type='file']")
    with tempfile.NamedTemporaryFile(suffix=".bin", delete=False) as f:
        f.write(b'Invalid file+')
        name = f.name
    inpt.send_keys(f.name)
    browser.find_element_by_xpath('//button[text()="Upgrade"]').click()
    browser.wait_until_xpath_selector("//*[text()='The selected image is not valid for the target device']")
